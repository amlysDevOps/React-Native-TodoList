import React from 'react';
import { View, ScrollView } from 'react-native';
import Header from './components/header';
import TaskList from './components/task-list';
import ButtonAddTask from './components/button-add-task';

const taskList = [
  {
    id: 0,
    content: 'Faire les courses',
    status: 'En cours'
  },
  {
    id: 1,
    content: 'Ne rien faire',
    status: 'Terminé'
  },
  {
    id: 2,
    content: 'regarder des séries sur Netflix',
    status: 'En cours'
  }
];

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = { taskList };
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header content="Liste des tâches" />
        <ScrollView>
          <TaskList taskList={this.state.taskList} />
        </ScrollView>
        <ButtonAddTask />
      </View>
    );
  }
}
