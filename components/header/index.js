import React from 'react';
import { Text, View } from 'react-native';
import { style } from './style';

//une variable qui contient une fonction, qui dit quand on appelle header tu appelle ça
//props c un mot clé pour dire les props passées quand on a appelé ce composant
//on peut appeler les arguments autrement avec {parm1, param2} etc.
const Header = props => (
  //props c global et '.' le param qu'on veut
  <View>
      <View style={style.subHeader} />
    <View style={style.header} >
      <Text style={style.text} >{props.content}</Text>
    </View>
  </View>
);

//faut bien l'exporter pour l'utiliser ailleurs
export default Header;
