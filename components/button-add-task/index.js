import React from 'react';
import ActionButton from 'react-native-action-button';
import { Icon } from 'react-native-elements';
import { APP_COLORS } from '../../styles/colors';

const ButtonAddTask = () => (
  <ActionButton
    buttonColor={APP_COLORS.primaryAction}
    icon={<Icon color={APP_COLORS.primaryText} name={'add'} />}
    onPress={() => onPressShowOk()}
  />
);


function onPressShowOk() {
  console.log('OK button action');
}
export default ButtonAddTask;
